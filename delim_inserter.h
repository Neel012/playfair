#ifndef DELIM_INSERTER_H
#define DELIM_INSERTER_H

#include <iterator>

#include "playfair.h"

template <typename OutputIterator>
struct delim_inserter
{
    using value_type = typename std::iterator_traits<OutputIterator>::value_type;
    using difference_type = typename std::iterator_traits<OutputIterator>::difference_type;
    using pointer = typename std::iterator_traits<OutputIterator>::pointer;
    using reference = typename std::iterator_traits<OutputIterator>::reference;
    using iterator_category = typename std::iterator_traits<OutputIterator>::iterator_category;
    using container_type = typename OutputIterator::container_type;
    using container_value_type = typename OutputIterator::container_type::value_type;

    delim_inserter(OutputIterator it, char delim, int count)
        : it(it), delim(delim), count(count) { }

    delim_inserter& operator=(const container_value_type& value)
    {
        if (count == 0) { it = delim; }
        count = count == 0 ? 4 : count - 1;
        it = value;
        return *this;
    }

//    delim_inserter& operator=(container_value_type&& value)
//    {
//        if (count == 0) { it = delim; }
//        count = count == 0 ? 4 : count - 1;
//        it = std::move(value);
//    }

    delim_inserter& operator*() { return *this; }
    delim_inserter& operator++() { return *this; }
    delim_inserter& operator++(int) { return *this; }

private:
    OutputIterator it;
    char delim;
    int count;
};

template <typename OutputIterator>
auto make_delim_inserter(OutputIterator it, char delim, int count)
{
    return delim_inserter<OutputIterator>(it, delim, count);
}

#endif // DELIM_INSERTER_H
