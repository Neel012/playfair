#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->menuBar->hide();
    ui->mainToolBar->hide();
    ui->statusBar->hide();
    ui->lineEditKey->setValidator(new QRegularExpressionValidator(QRegularExpression("[a-zA-Z]*"), this));
    ui->tableView->horizontalHeader()->hide();
    ui->tableView->verticalHeader()->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonEncode_clicked()
{
    Playfair p(ui->lineEditKey->text());
    if (p.key.size() < 5) {
        QMessageBox reject;
        reject.setText("Key must have at least 5 unique characters");
        reject.exec();
        return;
    }
    QStandardItemModel *model = new QStandardItemModel(5,5);
    for (unsigned row = 0; row < 5; ++row) {
        for (unsigned col = 0; col < 5; ++col) {
            QStandardItem *item = new QStandardItem(QString(p.m_table[row][col]));
            model->setItem(row, col, item);
        }
    }
    ui->tableView->setModel(model);
    ui->tableView->resizeColumnsToContents();
    auto encoded = p.encode(ui->plainTextEditInput->toPlainText());
    ui->plainTextEditOutput->setPlainText(encoded);
}

void MainWindow::on_pushButtonDecode_clicked()
{
    Playfair p(ui->lineEditKey->text());
    if (p.key.size() < 5) {
        QMessageBox reject;
        reject.setText("Key must have at least 5 unique characters");
        reject.exec();
        return;
    }
    QStandardItemModel *model = new QStandardItemModel(5,5);
    for (unsigned row = 0; row < 5; ++row) {
        for (unsigned col = 0; col < 5; ++col) {
            QStandardItem *item = new QStandardItem(QString(p.m_table[row][col]));
            model->setItem(row, col, item);
        }
    }
    ui->tableView->resizeColumnsToContents();
    auto decoded = p.decode(ui->plainTextEditInput->toPlainText());
    ui->plainTextEditOutput->setPlainText(decoded);
}
