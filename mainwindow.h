#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QRegularExpressionValidator>
#include <QMessageBox>
#include <QStandardItemModel>

#include "playfair.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButtonEncode_clicked();

    void on_pushButtonDecode_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
