#ifndef EVEN_INSERTER_H
#define EVEN_INSERTER_H

#include <iterator>
#include <experimental/optional>

template <typename OutputIterator>
struct even_inserter
{
    using value_type = typename std::iterator_traits<OutputIterator>::value_type;
    using difference_type = typename std::iterator_traits<OutputIterator>::difference_type;
    using pointer = typename std::iterator_traits<OutputIterator>::pointer;
    using reference = typename std::iterator_traits<OutputIterator>::reference;
    using iterator_category = typename std::iterator_traits<OutputIterator>::iterator_category;
    using container_type = typename OutputIterator::container_type;
    using container_value_type = typename OutputIterator::container_type::value_type;

    even_inserter(OutputIterator it) noexcept
        : m_it(it) { }

    even_inserter& operator*() { return *this; }
    even_inserter& operator++() { return *this; }
    even_inserter& operator++(int) { return *this; }

    even_inserter& operator=(const container_value_type &value)
    {
        if (m_value) {
            m_it = *m_value;
            m_it = value;
            m_value = std::experimental::nullopt;
        } else {
            m_value = value;
        }
        return *this;
    }

private:
    std::experimental::optional<container_value_type> m_value;
    OutputIterator m_it;
};

template <typename OutputIterator>
auto make_even_inserter(OutputIterator it)
{
    return even_inserter<OutputIterator>(it);
}

#endif // EVEN_INSERTER_H
