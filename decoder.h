#ifndef DECODER_H
#define DECODER_H

#include <experimental/optional>
#include <iterator>
#include <deque>

#include "playfair.h"

template <typename OutputIterator>
struct decoder
{
    using value_type = typename std::iterator_traits<OutputIterator>::value_type;
    using difference_type = typename std::iterator_traits<OutputIterator>::difference_type;
    using pointer = typename std::iterator_traits<OutputIterator>::pointer;
    using reference = typename std::iterator_traits<OutputIterator>::reference;
    using iterator_category = typename std::iterator_traits<OutputIterator>::iterator_category;
    using container_type = typename OutputIterator::container_type;
    using container_value_type = typename OutputIterator::container_type::value_type;

    decoder(OutputIterator it, const Playfair &playfair) noexcept
        : m_it(it), m_playfair(playfair) { }

    ~decoder() noexcept
    {
        std::copy(m_buf.begin(), m_buf.end(), m_it);
    }

    decoder& operator*() { return *this; }
    decoder& operator++() { return *this; }
    decoder& operator++(int) { return *this; }

    decoder& operator=(const container_value_type &value)
    {
        QChar a, b;
        if (value == ' ') {
            return *this;
        } else if (m_prev) {
            std::tie(a, b) = m_playfair.decode_digram(*m_prev, value);
            write(a);
            write(b);
            m_prev = std::experimental::nullopt;
        } else {
            m_prev = value;
        }
        return *this;
    }

private:
    void write(const container_value_type &value)
    {
        const QString &space = Playfair::s_space;
        const QString xqxq = QStringLiteral("XQXQ");
        if (int(m_buf.size()) < space.size()) {
            m_buf.push_back(value);
        } else {
            m_it = m_buf.front();
            m_buf.pop_front();
            m_buf.push_back(value);
        }
        if (std::equal(space.crbegin(), space.crend(), m_buf.crbegin(), m_buf.crend())) {
            // replace last 6 chars "XSPACE" -> " "
            m_it = ' ';
            m_buf.clear();
        } else if (match_from_back(xqxq)) {
            // replace last 4 chars "XQXQ" -> "XX"
            replace_last_four('X');
        } else if (match_cxcx()) {
            // replace last 4 chars "cXcX" -> "cc" where c is char from alphabet
            replace_last_four(*(m_buf.crbegin() + 1));
        }
    }

    bool match_cxcx()
    {
        if (m_buf.size() < 2) { return false; }
        QString cxcx = QStringLiteral("cXcX");
        QChar c = *(m_buf.crbegin() + 1);
        cxcx[0] = c;
        cxcx[2] = c;
        return match_from_back(cxcx);
    }

    bool match_from_back(const QString word)
    {
        return std::mismatch(word.crbegin(), word.crend(), m_buf.crbegin(), m_buf.crend()).first == word.crend();
    }

    void replace_last_four(const QChar &c)
    {
        for (int i = 0; i < 4; ++i) { m_buf.pop_back(); }
        m_buf.push_back(c);
        m_buf.push_back(c);
    }

private:
    std::deque<QChar> m_buf;
    OutputIterator m_it;
    const Playfair m_playfair;
    std::experimental::optional<container_value_type> m_prev;
};

template <typename OutputIterator>
auto make_decoder(OutputIterator it, const Playfair &playfair)
{
    return decoder<OutputIterator>(it, playfair);
}

#endif // DECODER_H
