#ifndef ENCODER_H
#define ENCODER_H

#include <iterator>
#include <experimental/optional>

#include "playfair.h"

template <typename OutputIterator>
struct encoder
{
    using value_type = typename std::iterator_traits<OutputIterator>::value_type;
    using reference = typename std::iterator_traits<OutputIterator>::reference;
    using pointer = typename std::iterator_traits<OutputIterator>::pointer;
    using difference_type = typename std::iterator_traits<OutputIterator>::difference_type;
    using iterator_category = typename std::iterator_traits<OutputIterator>::iterator_category;
    using container_type = typename OutputIterator::container_type;
    using container_value_type = typename OutputIterator::container_type::value_type;

    encoder(OutputIterator it, const Playfair &playfair)
        : m_it(it), m_playfair(playfair) { }

    ~encoder()
    {
        if (prev) {
            QChar pad = *prev != 'X' ? 'X' : 'Q';
            write(*prev, pad);
        }
    }

    encoder& operator=(const container_value_type& value)
    {
        if (!prev) {
            prev = value;
            return *this;
        }
        QChar a = *prev, b = value;
        if (a == b) {
            QChar pad = a != 'X' ? 'X' : 'Q';
            write(a, pad);
            write(b, pad);
        } else {
            write(a, b);
        }
        prev = std::experimental::nullopt;
        return *this;
    }

    encoder& operator*() { return *this; }
    encoder& operator++() { return *this; }
    encoder& operator++(int) { return *this; }

//    void swap(encoder& other) noexcept { std::swap(m_it, other.m_it); }

//    bool operator==(const encoder& rhs) { return m_it == rhs.m_it; }

//    bool operator!=(const encoder& rhs) { return !(*this == rhs); }
private:
    void write(QChar a, QChar b)
    {
        std::tie(a, b) = m_playfair.encode_digram(a, b);
        m_it = a;
        m_it = b;
    }

private:
    OutputIterator m_it;
    const Playfair &m_playfair;
    std::experimental::optional<container_value_type> prev;
};

template <typename OutputIterator>
auto make_encoder(OutputIterator it, const Playfair &playfair)
{
    return encoder<OutputIterator>(it, playfair);
}

#endif // SPACE_ENCODER_H
