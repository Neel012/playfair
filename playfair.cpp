#include <cassert>
#include <experimental/optional>
#include <iostream>

#include "delim_inserter.h"
//#include "even_inserter.h"
#include "playfair.h"
#include "decoder.h"
#include "encoder.h"

namespace {

QChar strip_diacritic(const QChar c)
{
    QString dia(QStringLiteral("ĚŠČŘŽÝÁÍÉŮÚ"));
    QString trim(QStringLiteral("ESCRZYAIEUU"));
    auto it = std::find(dia.cbegin(), dia.cend(), c.toUpper());
    if (it == dia.cend()) { return c; }
    return trim[uint(it - dia.cbegin())];
}

bool is_upper_letter(const QChar &in_c)
{
    auto c = in_c.unicode();
    return c >= 'A' && c <= 'Z';
}

struct upper_char_used
{
    bool &operator[](ushort c)
    {
        if (c > 'V') { --c; }
        return chars[c - ushort('A')];
    }
private:
    bool chars[25] = { false };
};

}

const QString Playfair::s_space = QStringLiteral("XSPACE");

Playfair::Playfair(const QString &key) noexcept
{
    m_table = generate_table(key);
}

auto Playfair::encode(const QString &in) const noexcept -> QString
{
    QString out;
    out.reserve(in.size() * 2);
    auto delim_out = make_delim_inserter(std::back_inserter(out), ' ', 5);
    auto it_out = make_encoder(delim_out, *this);
    for (const auto &x : in) {
        auto c = strip_diacritic(x).toUpper();
        if (c == ' ') {
            for (const auto &v : s_space) { it_out = v; }
        } else if (is_upper_letter(c)) {
            it_out = c;
        }
    }
    return out;
}

auto Playfair::decode(const QString &in) const noexcept -> QString
{
    QString out;
//    auto it_out = make_even_inserter(make_decoder(std::back_inserter(out), *this));
    auto it_out = make_decoder(std::back_inserter(out), *this);
    for (const auto &x : in) {
        it_out = x;
    }
    return out;
}

auto Playfair::generate_table(const QString &in_key) noexcept -> Playfair::Table
{
    Table table;
    key.reserve(in_key.size());
    // filter repeating chars
    upper_char_used char_used;
    for (auto v : in_key) {
        v = strip_diacritic(v).toUpper();
        auto c = v.unicode();
        if (!is_upper_letter(c)) { break; }
        if (c == 'W') { c = 'V'; }
        if (!char_used[c]) {
            key.append(c);
        }
        char_used[c] = true;
    }
    // create the playfair table
    auto k = key.cbegin();
    ushort a = 'A';
    for (auto &row : table) {
        for (auto &elem : row) {
            if (k != key.end()) {
                elem = *k;
                ++k;
            } else {
                a = a == 'W' ? a + 1 : a;
                while (char_used[a]) { ++a; }
                elem = a;
                ++a;
            }
        }
    }
    return table;
}

auto Playfair::encode_digram(QChar a, QChar b) const noexcept -> Digram
{
    point pa, pb;
    std::tie(pa, pb) = find_digram_in_table(a, b);
    if (pa.y == pb.y) {
        pa.x = (pa.x + 1) % 5;
        pb.x = (pb.x + 1) % 5;
    } else if (pa.x == pb.x) {
        pa.y = (pa.y + 1) % 5;
        pb.y = (pb.y + 1) % 5;
    } else {
        std::swap(pa.x, pb.x);
    }
    return std::make_pair(m_table[pa.y][pa.x], m_table[pb.y][pb.x]);
}

auto Playfair::decode_digram(QChar a, QChar b) const noexcept -> Digram
{
    point pa, pb;
    std::tie(pa, pb) = find_digram_in_table(a, b);
    if (pa.y == pb.y) {
        pa.x = pa.x == 0 ? 4 : (pa.x - 1) % 5;
        pb.x = pb.x == 0 ? 4 : (pb.x - 1) % 5;
    } else if (pa.x == pb.x) {
        pa.y = pa.y == 0 ? 4 : (pa.y - 1) % 5;
        pb.y = pb.y == 0 ? 4 : (pb.y - 1) % 5;
    } else {
        std::swap(pa.x, pb.x);
    }
    return std::make_pair(m_table[pa.y][pa.x], m_table[pb.y][pb.x]);
}

auto Playfair::find_digram_in_table(QChar a, QChar b) const noexcept -> std::pair<point, point>
{
    using std::experimental::optional;
    optional<point> pa, pb;
    for (unsigned i = 0; i < 5; ++i) {
        for (unsigned j = 0; j < 5; ++j) {
            if (m_table[i][j] == a) { pa = point{ j, i }; }
            if (m_table[i][j] == b) { pb = point{ j, i }; }
            if (pa && pb) { return std::make_pair(*pa, *pb); }
        }
    }
    assert(false);
}
