#ifndef PLAYFAIR_H
#define PLAYFAIR_H

// W -> V
// A B C D E
// F G H I J
// K L M N O
// P Q R S T
// U V X Y Z

#include <array>
#include <experimental/string_view>
#include <QString>
#include <QRegularExpression>
#include <utility>

struct Playfair
{
public:
    using Digram = std::pair<QChar, QChar>;
    using Table = std::array<std::array<QChar, 5>, 5>;

public:
    static const QString s_space;
//    static constexpr auto s_space = std::experimental::to_array("XSPACE");
//    static constexpr auto s_space = std::experimental::make_array("XSPACE");
//    static constexpr auto s_space = std::experimental::string_view("XSPACE");

    Playfair(const QString &key) noexcept;
    auto encode(const QString &in) const noexcept -> QString;
    auto decode(const QString &in) const noexcept -> QString;

    auto encode_digram(QChar a, QChar b) const noexcept -> Digram;
    auto decode_digram(QChar a, QChar b) const noexcept -> Digram;

    QString key;

private:
    struct point { unsigned x; unsigned y; };

private:
    auto generate_table(const QString &in_key) noexcept -> Playfair::Table;
    auto find_digram_in_table(QChar a, QChar b) const noexcept -> std::pair<point, point>;

public:
    Table m_table;
};

#endif // PLAYFAIR_H
